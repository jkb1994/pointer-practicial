//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include <stdio.h>

void changeValue(float *frequency);

int main (int argc, const char* argv[])
{

    float frequency;
    
    frequency = 100;
    printf("Outside: %.2f\n", frequency);
    
    changeValue(&frequency);
    printf("Outside: %.2f\n", frequency);
    
    
    return 0;
}

void changeValue(float *frequency)
{
    *frequency = 20;
    printf("Inside: %.2f\n", *frequency);
}